<?php

include('server.php');
require_once 'vendor/autoload.php';

$client   = new Kickbox\Client('live_09b805d4e558b34cb26b18534b332cf7b6ee1698c30f82374710d9d737d03a69');
$kickbox  = $client->kickbox();

			$email = "";
			if ($_SERVER["REQUEST_METHOD"] == "POST") {
                     $email = test_input($_POST["email"]);
                   function test_input($data) {
                              $data = trim($data);
                              $data = stripslashes($data);
                              $data = htmlspecialchars($data);
                   return $data;
                                               }
                                                      }
			try {
               $response = $kickbox->verify($email);
                 }
                catch (Exception $e) {
                 echo "Code: " . $e->getCode() . " Message: " . $e->getMessage();
                }     
?>


<!DOCTYPE html>
<html>
<head>
	<title>Registration system PHP and MySQL</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<div class="header">
		<h2>Register</h2>
	</div>
	
	<form method="post" action="register.php">

		<?php include('errors.php'); ?>

		<div class="input-group">
			<label>Username</label>
			<input type="text" name="username" value="<?php echo $username; ?>">
		</div>
		<div class="input-group">
			<label>Email</label>
			
			<input type="email" name="email" value="<?php echo $email;?>">
			
		</div>
		<div class="input-group">
			<label>Password</label>
			<input type="password" name="password_1">
		</div>
		<div class="input-group">
			<label>Confirm password</label>
			<input type="password" name="password_2">
		</div>
		<div class="input-group">
			<button type="submit" class="btn" name="reg_user">Register</button>
		</div>
		<p>
			Already a member? <a href="login.php">Sign in</a>
		</p>
	</form>
</body>
</html>
<?php include('server.php');


require_once 'vendor/autoload.php';

$client   = new Kickbox\Client('live_09b805d4e558b34cb26b18534b332cf7b6ee1698c30f82374710d9d737d03a69');
$kickbox  = $client->kickbox();

			$email = "";
			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				
				function test_input($data) {
                              $data = trim($data);
                              $data = stripslashes($data);
                              $data = htmlspecialchars($data);
                   return $data;
                                          }
			try {
               $response = $kickbox->verify($email);
                 }
                catch (Exception $e) {
                 echo "Code: " . $e->getCode() . " Message: " . $e->getMessage();
                }
			}
				
				
				
	if(isset($_POST['login_user'])) {
		// print_r($_POST);
		$url = "https://www.google.com/recaptcha/api/siteverify";
		$data = [
			'secret' => "6LfRWMYUAAAAAPHe_eI2zJo6QCsj61LgVf6F6Xbs",
			'response' => $_POST['token'],
			// 'remoteip' => $_SERVER['REMOTE_ADDR']
		];
		$options = array(
		    'http' => array(
		      'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
		      'method'  => 'POST',
		      'content' => http_build_query($data)
		    )
		  );
		$context  = stream_context_create($options);
  		$response = file_get_contents($url, false, $context);
		$res = json_decode($response, true);
		if($res['success'] == true) {
			// Perform you logic here for ex:- save you data to database
  			echo '<div class="alert alert-success">
			  		<strong>Success!</strong> Your inquiry successfully submitted.
		 		  </div>';
		} else {
			echo '<div class="alert alert-warning">
					  <strong>Error!</strong> You are not a human.
				  </div>';
		}
	}
 ?>
 
 
<!DOCTYPE html>
<html>
<head>
	<title>Registration system PHP and MySQL</title>
	<link rel="stylesheet" type="text/css" href="../library/css/bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" href="style.css">
	 <script src="https://www.google.com/recaptcha/api.js?render=6LfRWMYUAAAAACIEEhlxYuP50SMpfADK0ARYqZrZ"></script>
</head>
<body>

	<div class="header">
		<h2>Login</h2>
	</div>
	
	<form method="post" action="login.php">

		<?php include('errors.php'); ?>

		<div class="input-group">
			<label>Username</label>
			<input type="text" name="username" >
		</div>
		<div class="input-group">
			<label>Password</label>
			<input type="password" name="password">
		</div>
		<div class="input-group">
			<input type="submit" class="btn" name="login_user" value="login"></input>
		</div>
		<input type="hidden" id="token" name="token">
		<p>
			Not yet a member? <a href="register.php">Sign up</a>
		</p>
	</form>
</body>

 <script>
  grecaptcha.ready(function() {
      grecaptcha.execute('6LfRWMYUAAAAACIEEhlxYuP50SMpfADK0ARYqZrZ', {action: 'homepage'}).then(function(token) {
         // console.log(token);
         document.getElementById("token").value = token;
      });
  });
  </script>
 
</html>